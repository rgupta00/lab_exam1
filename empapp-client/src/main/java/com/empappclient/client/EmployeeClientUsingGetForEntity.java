package com.empappclient.client;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class EmployeeClientUsingGetForEntity {

	@Autowired
	private RestTemplate restTemplate;
//
//	@GetMapping("get-client")
//	public Employee client() {
//		String url="http://localhost:8080/empapp/employee/4";
//		
//		ResponseEntity<Employee>empResponse=restTemplate
//				.getForEntity(url, Employee.class);
//		
//		System.out.println("-----some valuable infor from empResponse------");
//		System.out.println(empResponse.getStatusCode());
//		System.out.println(empResponse.getHeaders());
//		
//		Employee employee=empResponse.getBody();//u get body from the response object
//		
//		return employee;
//		
//	}
	

//	@GetMapping("get-client")
//	public List client() {
//		String url="http://localhost:8080/empapp/employee";
//		
//		ResponseEntity<List>empResponse=restTemplate
//				.getForEntity(url, List.class);
//		
//		System.out.println("-----some valuable infor from empResponse------");
//		System.out.println(empResponse.getStatusCode());
//		System.out.println(empResponse.getHeaders());
//		
//		List employees=empResponse.getBody();//u get body from the response object
//		
//		return employees;
//		
//	}
	
//	@GetMapping("add-client")
//	public Employee client() {
//		String url="http://localhost:8080/empapp/employee";
//		
//		Employee employeeToAdd=new Employee("neeraj", 23);
//		
//		ResponseEntity<Employee>empResponse=restTemplate
//				.postForEntity(url, employeeToAdd, Employee.class);
//		
//		System.out.println("-----some valuable infor from empResponse------");
//		System.out.println(empResponse.getStatusCode());
//		System.out.println(empResponse.getHeaders());
//		
//		Employee employee=empResponse.getBody();//u get body from the response object
//		
//		return employee;
//		
//	}
//	
	
//	
//	@GetMapping("update-client")
//	public String client() {
//
//        Employee employee=new Employee("keshav", 8);
//        restTemplate.put("http://localhost:8080/empapp/employee/4", employee);
//       	System.out.println("updated....");
//
//		return "done";
//	}
//	
	
}
